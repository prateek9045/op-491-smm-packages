from __future__ import unicode_literals

import ast
import importlib
import urllib
import logging
import urlparse

#from allauth.socialaccount.models import SocialAccount
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.crypto import get_random_string
from rest_framework import serializers

from smm_app.scheduler.helper import update_asset_log, is_media_type_allowed, get_client_account, \
    retrieve_social_member_accounts, get_current_formatted_datetime
from emam_middleware import settings
from smm_app.models import UploadAssetDetail, MetaData, AssetDetail

User = get_user_model()

# Get an instance of a logger
logger = logging.getLogger(__name__)

class MetaDataSerializer(serializers.Serializer):
    sample_metadata = {
        'title': '',
        'file_name': '',
        'file_size': '',
        'file_description': '',
        'asset_id': '',
        'low_res_virtual_path': '',
        'full_low_res_virtual_path': '',
        'high_res_virtual_path': '',
        'full_high_res_virtual_path': '',
        'low_res_folder_path': '',
        'full_low_res_folder_path': '',
        'full_high_res_folder_path': '',
        'thumbnail_path': '',
        'emam_user': '',
        'social_platform': '',
        'client_account_id': '',
        'media_type': '',
    }

    ''' Validations
    default: required = true, allow_blank=False
    required:True: means field should be present and have to be filled
    required:False: means field may not be present, but if present it has to be filled
    '''

    title = serializers.CharField(max_length=254)
    file_name = serializers.CharField(max_length=254, allow_blank=True)
    file_size = serializers.CharField(max_length=20, allow_blank=True)
    file_description = serializers.CharField(max_length=254, allow_blank=True)
    asset_id = serializers.CharField(max_length=100)

    social_platform = serializers.CharField(max_length=150, required=False, default='', allow_blank=True)
    client_account_id = serializers.CharField(max_length=100, required=False, default='', allow_blank=True)

    # Non model fields
    low_res_virtual_path = serializers.CharField(max_length=254, allow_blank=True)
    full_low_res_virtual_path = serializers.CharField(max_length=254, allow_blank=True)
    high_res_virtual_path = serializers.CharField(max_length=254, required=False, default='', allow_blank=True)
    full_high_res_virtual_path = serializers.CharField(max_length=254, allow_blank=True)
    thumbnail_path = serializers.CharField(max_length=254, allow_blank=True)
    low_res_folder_path = serializers.CharField(max_length=254, allow_blank=True)
    full_low_res_folder_path = serializers.CharField(max_length=254, allow_blank=True)
    full_high_res_folder_path = serializers.CharField(max_length=254, allow_blank=True)
    emam_user = serializers.CharField(max_length=254)
    # For Getty, submission-type like 'getty_editorial_still', 'getty_creative_still', 'getty_editorial_video' etc..
    media_type = serializers.CharField(max_length=50, required=False, default='', allow_blank=True)

    def create(self, validated_data):

        """
        Create metadata record with related assets to be uploaded

        :param validated_data:
        :return:
        """

        try:
            count = 0
            user_id = get_user(validated_data=validated_data)

            # Invalid emam user
            if user_id == -1:
                return self.sample_metadata
            if validated_data.get('client_account_id'):
                try:
                    from brightcove_app.scheduler import brightcove
                except ImportError as ex:
                    count = count + 1
                    logger.exception(ex)
            elif validated_data.get('social_platform'):
                try:
                    social_platform = validated_data.get('social_platform')
                    importlib.import_module('smm_app.scheduler.'+str(social_platform))
                except ImportError as ex:
                    count = count + 1
                    logger.exception(ex)
            if count == 0:
                metadata = MetaData.objects.create(
                    title=validated_data.get('title'),
                    file_name=validated_data.get('file_name'),
                    file_size=validated_data.get('file_size'),
                    file_description=validated_data.get('file_description'),
                    asset_id=validated_data.get('asset_id'),
                    emam_user=user_id,
                    social_platform=validated_data.get('social_platform'),
                    client_account_id=validated_data.get('client_account_id'),
                )

                if validated_data.get('full_low_res_virtual_path'):
                    asset_detail = AssetDetail.objects.create(
                        metadata=metadata,
                        asset_type=AssetDetail.AssetType.LOW_RESOLUTION.value,
                        asset_source_url=validated_data.get('full_low_res_virtual_path')
                    )
                    if not validated_data.get('full_high_res_virtual_path'):
                        save_upload_asset_detail(asset_detail_object=asset_detail, validated_data=validated_data, http_asset=True)

                if validated_data.get('full_low_res_folder_path'):
                    asset_detail = AssetDetail.objects.create(
                        metadata=metadata,
                        asset_type=AssetDetail.AssetType.LOW_RESOLUTION_SOURCE_FILE.value,
                        asset_source_url=validated_data.get('full_low_res_folder_path'),
                    )
                    if not validated_data.get('full_high_res_folder_path'):
                        save_upload_asset_detail(asset_detail_object=asset_detail, validated_data=validated_data)

                if validated_data.get('full_high_res_virtual_path'):
                    asset_detail = AssetDetail.objects.create(
                        metadata=metadata,
                        asset_type=AssetDetail.AssetType.HIGH_RESOLUTION.value,
                        asset_source_url=validated_data.get('full_high_res_virtual_path'),
                    )
                    save_upload_asset_detail(asset_detail_object=asset_detail, validated_data=validated_data, http_asset=True)

                if validated_data.get('full_high_res_folder_path'):
                    asset_detail = AssetDetail.objects.create(
                        metadata=metadata,
                        asset_type=AssetDetail.AssetType.HIGH_RESOLUTION_SOURCE_FILE.value,
                        asset_source_url=validated_data.get('full_high_res_folder_path'),
                    )
                    save_upload_asset_detail(asset_detail_object=asset_detail, validated_data=validated_data)

                if validated_data.get('thumbnail_path'):
                    low_res_path = None
                    if validated_data.get('low_res_virtual_path'):
                        low_res_path = urlparse.unquote(validated_data.get('low_res_virtual_path')) + "/"
                    if validated_data.get('low_res_folder_path'):
                        low_res_path = urlparse.unquote(validated_data.get('low_res_folder_path')) + "\\"

                    if low_res_path is not None:
                        AssetDetail.objects.create(
                            metadata=metadata,
                            asset_type=AssetDetail.AssetType.THUMBNAIL.value,
                            asset_source_url=urllib.quote_plus(
                                urlparse.unquote(low_res_path) +
                                urlparse.unquote(validated_data.get('thumbnail_path'))
                            ),
                        )
        except Exception as ex:
            logger.exception(ex)

        return self.sample_metadata

def get_user(validated_data):
    """
    Deprecated:
    If emam_user given in metadata but doesn't recognized by server - Reject Metadata with error log

    NEW:
    If emam_user given in metadata doesn't recognized by server - Create new User with provided
    username with random password.
    if eMAM user logs in to Middleware front-end app anytime with the same username and actual eMAM password,
    then user will be authenticated via eMAM auth API and replace the random password with correct eMAM user
    password.

    :return:None|Int
    """
    result = None
    if validated_data.get('emam_user'):
        try:
            emam_user = User.objects.get(username=validated_data.get('emam_user'))
            result = emam_user.id
        except User.DoesNotExist:
            # Create eMAM user with provided username with random password if does not exist.
            user = User.objects.create_user(
                username=validated_data.get('emam_user'), password=get_random_string(length=10)
            )
            result = user.id
            try:
                from smm_app.models import SocialAccountMembers
                # Check if user is linked with some social account then replace its user id.
                user_obj = SocialAccountMembers.objects.filter(user=None, user_username=validated_data.get('emam_user'))
                for x in user_obj:
                    x.user = user
                    x.save()
            except Exception as ex:
                logger.error(ex)

            '''log_message = "The user '{0}' could not be recognized by the server ".format(
                validated_data.get('emam_user')
            )
            logger.error(log_message)
            result = -1'''
        except Exception as ex:
            logger.exception(ex)
            result = -1
    return result

def save_upload_asset_detail(asset_detail_object, validated_data, http_asset=False):
    """
    -- Segregate metadata upload request into multiple records as per the requested social platforms --

    1) Check eMAM-user existence in database
    2) Check social-media provider in the server against the user requested social platform, if not
       exist then log error in asset-log table
    3) Create separate records in upload-asset-detail table, to be uploaded on each social account
       of the user w.r.t social platform

    :param bool http_asset:
    :param AssetDetail asset_detail_object:
    :return:
    """
    try:
        user_id = get_user(validated_data=validated_data)
        if user_id > 0 and validated_data.get('social_platform'):

            social_platforms = validated_data.get('social_platform')
            for social_platform in social_platforms.split(','):

                if social_platform != '':
                    try:
                        social_platform = str(social_platform).strip().lower()
                        upload_api_service = settings.UPLOAD_API_SERVICE[social_platform]
                        if social_platform == 'getty':
                            try:
                                from getty_app.service import getty_request
                                getty_request(asset_detail_object, http_asset, social_platform, user_id)
                            except ImportError as ex:
                                logger.exception(ex)
                                logger.error('Requested platform app is not installed yet.')
                        else: # for other social_platforms
                            from smm_app.service import social_request
                    except KeyError:
                        # Invalid social platform
                        log_message = "[Error] [{0}] User requested social platform '{1}' could not be " \
                                      "recognized by the server.".format(
                                        get_current_formatted_datetime(), social_platform
                                        )
                        update_asset_log(asset_detail_object, log_message)
                        logger.error("User requested social platform '{0}' could not be recognized "
                                     "by the server.".format(social_platform))
                        pass
        if validated_data.get('client_account_id'): # Brightcove case
            try:
                import brightcove_app.service as brightcove_service
                result = brightcove_service.brightcove_request(validated_data=validated_data, http_asset=http_asset)
                if result:
                    if is_media_type_allowed(asset_detail_object.asset_source_decoded_url, result[3]):
                        UploadAssetDetail.objects.create(
                            asset_detail=asset_detail_object,
                            upload_to=result[2].Client.BRIGHTCOVE.value,
                            upload_to_account=validated_data.get('client_account_id')
                        )
                    logger.info(result[1])
            except ImportError as ex:
                logger.exception(ex)
                logger.error('Requested "Brightcove" app is not installed yet.')

        # Workflow client-name = setting.upload-service.client.client-name and client-accounts-client-name

    except Exception as ex:
        logger.exception(ex)

class UserSerializer(serializers.ModelSerializer):

    full_name = serializers.CharField(source='get_full_name', read_only=True)

    class Meta:
        model = User
        fields = ('id', User.USERNAME_FIELD, 'full_name', 'is_active', )
