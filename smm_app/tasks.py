from __future__ import unicode_literals

import importlib
from celery.decorators import periodic_task
#from celery.task.schedules import crontab
import logging

from celery.schedules import crontab
from django.utils import timezone

from smm_app.models import get_current_formatted_datetime, UploadAssetDetail
from smm_app.scheduler import helper
from emam_middleware import settings
import json
import concurrent.futures

# Get an instance of a logger
logger = logging.getLogger(__name__)


@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
def handler():
    try:
        assets = helper.fetch_assets_to_upload()
        if assets:
            with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
                executor.map(uploader, assets)
    except Exception as ex:
        logger.exception(ex)


def uploader(asset):
    """
    Take assets from handler in a multi-threaded way and call relevant api service to upload

    :param string asset: 'metadata-title|asset-url|upload-asset-id|client-account-id:optional|asset-type|upload-to'
    :return:
    """

    asset_data = asset.split("|")
    upload_asset_detail_id = int(asset_data[2])
    upload_to = asset_data[5]
    error = None
    upload_status = None
    api_service = 'Unknown'


    try:
        try:
            #logger.error(upload_to)
            api_service = settings.UPLOAD_API_SERVICE[upload_to]['upload_handler']
        except KeyError:
            api_service = settings.UPLOAD_API_SERVICE['client'][upload_to]['upload_handler']
        if api_service != 'facebook':
            #logger.error(upload_to + '.scheduler.' + api_service)
            module = importlib.import_module( upload_to + '_app.scheduler.' + api_service)
            if upload_to == "brightcove":
                log_message = "[{0}] Initiated process for uploading an asset.".format(
                    get_current_formatted_datetime())
                asset_obj = helper.update_asset_status(
                        upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADING.value, log_message)
                result = getattr(module, 'uploader')(asset_details=asset, asset=asset_obj)
            else:
                result = getattr(module, 'uploader')(asset)
            update_status_and_logs(result=result)
            #return [update_log, status, upload_asset_detail_id, emam_asset_id, video_id ]
    except KeyError:
        error = "[Severe] [{0}] Upload destination `{1}`, was unknown".format(
            get_current_formatted_datetime(), upload_to
        )
        upload_status = UploadAssetDetail.UploadStatus.INVALID.value
    except AttributeError as ex:
        error = "[Severe] [{0}] Upload Handler not found for `{1}` upload: {2}".format(
            get_current_formatted_datetime(), upload_to, ex
        )
        upload_status = UploadAssetDetail.UploadStatus.ERROR.value
    except ImportError as ex:
        logger.error(ex)
        error = "[Severe] [{0}] No module named `{1}`".format(
            get_current_formatted_datetime(), api_service
        )
        upload_status = UploadAssetDetail.UploadStatus.INVALID.value
    except Exception as ex:
        logger.exception(ex)

    if error is not None:
        helper.update_asset_status(upload_asset_detail_id, upload_status, error)


@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
def update_upload_status():

    """
    For Client (i.e BrightCove) uploads only
    Update upload status in a database acknowledged by client
    :return:
    """
    entity_ids_to_be_cleared = []
    try:
        with open(settings.UPLOAD_LOG_FILE, 'r+') as get_log_json:
            try:
                upload_status_data = json.load(get_log_json)
                asset_object = UploadAssetDetail.objects.filter(
                    upload_status=UploadAssetDetail.UploadStatus.UPLOADING.value
                )

                for asset in asset_object:
                    video_id = asset.uploaded_media_id
                    if video_id in upload_status_data:
                        upload_status = upload_status_data[video_id]
                        if upload_status == 'SUCCESS':
                            uploaded_status = UploadAssetDetail.UploadStatus.UPLOADED.value

                            log_message = "[{0}] Asset uploaded successfully to client location.".format(
                                get_current_formatted_datetime()
                            )
                            asset.uploaded_at = timezone.now()
                        else:
                            uploaded_status = UploadAssetDetail.UploadStatus.INVALID.value

                            log_message = "[{0}] Asset could not be uploaded by client.".format(
                                get_current_formatted_datetime()
                            )

                        asset.upload_status = uploaded_status
                        asset.message = asset.message + log_message + "\n"
                        asset.save()

                        entity_ids_to_be_cleared.append(video_id)
            except ValueError as ex:
                logger.error(ex)
    except IOError as ex:
        logger.error(ex)
    except Exception as ex:
        logger.exception(ex)

    helper.update_upload_status_log(entity_ids_to_be_cleared=entity_ids_to_be_cleared)


def update_status_and_logs(result):
    #logger.error('0 :: {0}'.format(result[0]))
    #logger.error('1 :: {0}'.format(result[1]))
    #logger.error('2 :: {0}'.format(result[2]))
    #logger.error('3 :: {0}'.format(result[3]))
    #logger.error('4 :: {0}'.format(result[4]))

    #[update_log, status, upload_asset_detail_id, emam_asset_id, video_id]
    try:
        try:
            logger.error(result[1][-1])
            if result[0] and result[1][-1] == 'invalid' and not result[2] and not result[3] and result[4]: # 1st and 5th
                helper.update_asset_status(upload_asset_detail_id=result[0], upload_status=UploadAssetDetail.UploadStatus.INVALID.value, log_message=result[4])

            if result[0] and result[1][-1]=='n/a' and not result[2] and not result[3] and result[4]: # 2nd
                helper.update_asset_status(result[0], UploadAssetDetail.UploadStatus.NA.value, result[4])

            if result[0] and result[1][-1]=='error' and not result[2] and not result[3] and result[4]: # 3rd
                helper.update_asset_status(result[0], UploadAssetDetail.UploadStatus.ERROR.value, result[4])

            if result[0] and result[1][-1] == 'invalid' and result[2] and not result[3] and not result[4]:  # 7th
                if type(result[2]) == UploadAssetDetail:
                    result[2].upload_status = UploadAssetDetail.UploadStatus.INVALID.value
                    result[2].save()
        except Exception as ex:
            #logger.error(ex)
            if result[0] and not result[1] and result[2] and result[3] and result[4]: # 4th
                helper.update_upload_log(result[0], result[4])
                helper.send_media_upload_id(result[0], result[2], result[3])

            if result[0] and not result[1] and result[2] and not result[3] and result[4]: # 6th
                logger.error('6th..........')
                logger.error(result[2])
                if type(result[2]) == UploadAssetDetail:
                    result[2].upload_status = UploadAssetDetail.UploadStatus.INVALID.value
                    result[2].save()
                    logger.error(result[2])
                helper.update_upload_log(result[0], result[4])

    except Exception as ex:
        logger.error(ex)

