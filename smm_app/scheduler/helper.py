import mimetypes
import time
import json
from django.contrib import messages
import errno
from django.contrib.auth import logout
from django.shortcuts import redirect

from smm_app.models import *
from django.db.models import Q
import socket
import requests
import logging
from smm_app.emam_gateway import EMAMGateway
from emam_middleware import settings
# Get an instance of a logger
logger = logging.getLogger(__name__)


def download_asset_to_upload(asset_detail_id, upload_asset_detail_id, asset_source_decoded_url):
    """
    Download asset first in order to upload it to another server( - mainly social media platform)

    :param int asset_detail_id:
    :param int upload_asset_detail_id:
    :param string asset_source_decoded_url:
    :return: downloaded-file-path
    """

    asset_file_name = asset_source_decoded_url.split("/")[-1]

    download_to = os.path.join(
       download_directory_path(asset_detail_id, True),
       asset_file_name
    )

    is_downloaded = False

    for i in range(settings.DOWNLOAD_MAX_RETRIES):
        try:
            response = requests.get(asset_source_decoded_url, stream=True, timeout=10)
            # Handle bad http errors
            response.raise_for_status()

            with open(download_to, 'wb') as f:
                for chunk in response.iter_content(chunk_size=1024):
                    # dl += len(chunk)
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)

                log_message = '[{0}] Asset downloaded successfully.'.format(get_current_formatted_datetime())
                update_upload_log(upload_asset_detail_id, log_message)
                is_downloaded = True
            # Break max-retry loop if downloaded successfully
            break
        except requests.exceptions.ConnectionError:
            # Raised when http connection failed
            # Log message in database if http connection failed
            log_message = "[Error] [{0}] Http Connection failed while downloading asset. " \
                          "Retrying in {1} seconds".format(
                                get_current_formatted_datetime(), settings.DOWNLOAD_NEXT_RETRY_IN
                            )
            update_upload_log(upload_asset_detail_id, log_message)

        except socket.timeout:
            # Raised when connection lost while streaming content from http url
            # Log message in database
            log_message = "[Error] [{0}] Http Connection socket timeout. " \
                          "Retrying after {1} seconds".format(
                                get_current_formatted_datetime(), settings.DOWNLOAD_NEXT_RETRY_IN
                            )
            update_upload_log(upload_asset_detail_id, log_message)

        time.sleep(settings.DOWNLOAD_NEXT_RETRY_IN)
    else:
        # Connection error, max-retry exceeded
        log_message = "[Severe] [{0}] Download failed! Max retry reached!".format(
            get_current_formatted_datetime()
        )
        update_asset_status(upload_asset_detail_id, UploadAssetDetail.UploadStatus.ERROR.value, log_message)

    if is_downloaded is True:
        return download_to
    else:
        return None


def update_asset_status(
    upload_asset_detail_id,
    upload_status=None,
    log_message=None,
    uploaded_media_id=None,
):
    """
    Update upload asset status with status/log/video-id/uploaded-at

    :param int upload_asset_detail_id:
    :param string upload_status:
    :param string log_message:
    :param string uploaded_media_id:
    :return: (object) UploadAssetDetail
    """
    try:
        upload_asset_detail = UploadAssetDetail.objects.get(id=upload_asset_detail_id)

        if upload_status is not None:
            upload_asset_detail.upload_status = upload_status

        if log_message is not None:
            upload_asset_detail.message = upload_asset_detail.message + log_message + "\n"

        if uploaded_media_id is not None:
            upload_asset_detail.uploaded_media_id = uploaded_media_id
            upload_asset_detail.uploaded_at = timezone.now()

        upload_asset_detail.save()

        return upload_asset_detail
    except Exception as ex:
        logger.exception(ex)


def get_client_account(account_id, client_name='', ClientAccounts=None):
    """
    Get client account detail from database

    :param string client_name:
    :param string account_id:
    :return:
    :exception DoesNotExist
    """

    client_name = client_name or ClientAccounts.Client.BRIGHTCOVE.value

    return ClientAccounts.objects.get(account_id=account_id, client_name=client_name)


def is_media_type_allowed(resource, upload_api_service):
    """
    Check if received asset is applicable for requested social platform

    :param string resource:
    :param dict upload_api_service:
    :return: bool
    """
    try:
        mime_type = guess_mime_type(resource)
        if 'allowed_media' not in upload_api_service or upload_api_service['allowed_media'] == mime_type:
            return True
    except Exception:
        pass

    logger.error('Invalid extension of asset: {0}.'.format(resource))

    return False


def guess_mime_type(media_file):
    """
    Guess mime type of a given file
    :param string media_file:
    :return:
    """
    mime_type = mimetypes.guess_type(media_file)[0]
    if mime_type is not None:
        return mime_type.split('/')[0]
    return None


def update_upload_log(upload_asset_detail_id, log_message):
    """
    Update upload-asset log message in database

    :param int upload_asset_detail_id:
    :param str log_message:
    :return: (object) UploadAssetDetail
    """

    upload_asset_detail = UploadAssetDetail.objects.get(id=upload_asset_detail_id)
    upload_asset_detail.message = upload_asset_detail.message + log_message + "\n"
    upload_asset_detail.save()

    return upload_asset_detail


def update_asset_log(asset_detail, log_message):
    """
    Add/Update asset log message in database

    :param AssetDetail asset_detail:
    :param str log_message:
    :return: (object) AssetLog
    """

    try:
        asset_log = AssetLog.objects.get(asset_detail=asset_detail)
        asset_log.message = asset_log.message + log_message + "\n"
        asset_log.save()
    except AssetLog.DoesNotExist:
        asset_log = AssetLog.objects.create(asset_detail=asset_detail, message=log_message)

    return asset_log


def silent_remove(filename):
    """
    Remove downloaded asset from local filesystem after successful upload on social platform

    :param string filename:
    :return:
    """
    try:
        os.remove(filename)
    except OSError as e:
        # errno.ENOENT means "no such file or directory"
        if e.errno != errno.ENOENT:
            logger.exception(e)


def fetch_assets_to_upload():
    """
    Fetch assets to be uploaded

    :return: (list) assets
    """
    assets = []

    try:
        # Fetch assets for uploading to client (Brightcove)
        upload_asset_object = UploadAssetDetail.objects.filter(
            Q(upload_status=UploadAssetDetail.UploadStatus.PENDING.value) |
            Q(upload_status=UploadAssetDetail.UploadStatus.ERROR.value)
        )

        for asset in upload_asset_object:
            applicable_to_upload = False
            if asset.upload_to == UploadAssetDetail.UploadTo.CLIENT.value:
                # Request for Client Upload
                account_id = asset.asset_detail.metadata.client_account_id

                if not account_id:
                    log_message = "[{0}] No client account id for this upload request metadata.".format(
                        get_current_formatted_datetime()
                    )
                    asset.upload_status = UploadAssetDetail.UploadStatus.INVALID.value
                    asset.message = asset.message + log_message + "\n"
                    asset.save()
                else:
                    applicable_to_upload = True
            else:
                # Request for Social Media Upload

                ''' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                           TO DO
                    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    1) if asset.upload_to is not in [accepted-social-platforms]:
                          upload-status will changes to "Invalid" with log message

                    2) Fetch emam-user specific social credentials from database
                '''
                # Assuming for YouTube only as of now.
                applicable_to_upload = True

            if applicable_to_upload:
                assets.append('{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}'.format(
                    asset.asset_detail.metadata.title, asset.asset_detail.asset_source_decoded_url,
                    asset.id, asset.asset_detail.metadata.client_account_id, asset.asset_detail.asset_type,
                    asset.upload_to, asset.social_account_id, asset.asset_detail.id,
                    asset.asset_detail.metadata.emam_user, asset.asset_detail.metadata.file_description,
                    asset.asset_detail.asset_source_unquoted_url, asset.asset_detail.metadata.asset_id,
                    asset.media_type, asset.asset_detail.metadata.file_name
                ))
    except Exception as ex:
        logger.exception(ex)

    return assets


def update_upload_status_log(entity_data=None, entity_ids_to_be_cleared=None):

    """
    Delete acknowledged entities status and update new one

    :param dict entity_data:
    :param list entity_ids_to_be_cleared:
    :return:
    """

    with open(settings.UPLOAD_LOG_FILE, 'a+') as upload_log_json:
        try:
            log_data = json.load(upload_log_json)
        except ValueError:
            log_data = {}

    with open(settings.UPLOAD_LOG_FILE, 'w') as upload_log_json:
        if entity_data is not None:
            log_data.update(entity_data)

        # Remove archived entities from upload log
        if entity_ids_to_be_cleared is not None:
            for archived_entity in entity_ids_to_be_cleared:
                log_data.pop(archived_entity, None)

        json.dump(log_data, upload_log_json)


def send_media_upload_id(upload_asset_detail_id, asset_id, field_value):
    """ Update media upload_id generated by BrightCove on eMAM Server

    :param int upload_asset_detail_id:
    :param string asset_id: eMAM asset id provided by eMAM Server on metadata trigger
    :param string field_value: upload id
    :return:
    """

    try:
        emam_gateway = EMAMGateway()
        if emam_gateway.authenticate_user(
            settings.EMAM_USERNAME, settings.EMAM_USER_PASSWORD, settings.EMAM_UNIT_ID, settings.EMAM_LICENSE_KEY
        ):
            emam_gateway.recursive_dict(emam_gateway.get_custom_metadata(asset_id, settings.EMAM_UNIT_ID))
            if emam_gateway.upload_id:
                field_value = '{0},{1}'.format(emam_gateway.upload_id, field_value)
            emam_gateway.update_asset_metadata(asset_id, [[settings.EMAM_UPLOAD_FIELD_ID, field_value]])
    except Exception as ex:
        logger.exception(ex)

        log_message = "[Warning] [{0}] Could not update upload-id on asset source server.".format(
            get_current_formatted_datetime()
        )
        update_upload_log(upload_asset_detail_id, log_message)


# Exclude linked users
def exclude_added_users(excluded_current_user_and_admin, linked_user_list):
    excluded_users = []
    try:
        if len(linked_user_list)>0:
            for user in excluded_current_user_and_admin:
                excluded_users.append(user)
            for x in linked_user_list:
                for user in excluded_users:
                    if not x.user:
                        user_email = str(x.user_username)
                    else:
                        user_email = str(x.user.username)
                    if str(user) == user_email:
                        excluded_users.remove(user)
        else:
            excluded_users.append(excluded_current_user_and_admin)
            return excluded_users[0]
        return excluded_users
    except Exception as ex:
        logger.error(ex)


# Retrieving social member accounts
def retrieve_social_member_accounts(user_id, upload_api_service_provider):
    try:
        social_member_accounts = SocialAccountMembers.objects.filter(user_id=user_id, is_active=1)
        social_account_ids = []
        for social_member_account in social_member_accounts:
            social_account_ids.append(social_member_account.social_account_id)

        social_accounts = SocialAccount.objects.filter(
            Q(Q(user_id=user_id) | Q(id__in=social_account_ids))
            & Q(provider=upload_api_service_provider)
        )
        return social_accounts
    except Exception as ex:
        logger.error(ex)


# Restricting superuser to login on front-end panel.
def restrict_superuser_to_login(request):
    superusers_usernames = User.objects.filter(is_superuser=True).values_list('username')
    for superusers_username in superusers_usernames:
        if str(superusers_username[0]) == str(request.user.username):
            logout(request)
            messages.error(request, "You don't have access to this panel.")
            return redirect('login')