from __future__ import unicode_literals

import json
import logging
import requests
from allauth.socialaccount.models import SocialToken

from django.core.exceptions import ObjectDoesNotExist

#from emam.scheduler import helper
#from emam.models import *
from smm_app import settings


from smm_app.service import get_current_formatted_datetime, is_media_type_allowed

# Get an instance of a logger
logger = logging.getLogger(__name__)
LINKED_IN_API_URL = 'https://api.linkedin.com/v1/people/~/shares?format=json'
asset_status =[]
log_messages = []

def uploader(asset):
    """
    Upload assets on client's VideoCloud
    :param asset:
    """
    upload_asset_detail_id = None
    try:
        asset_data = asset.split("|")
        asset_title = asset_data[0]
        asset_source_decoded_url = asset_data[1]

        upload_asset_detail_id = asset_data[2]
        upload_to = asset_data[5]
        social_account_id = asset_data[6]
        asset_description = asset_data[9]

        upload_api_service = settings.UPLOAD_API_SERVICE[upload_to]

        if not is_media_type_allowed(asset_source_decoded_url, upload_api_service):
            raise Exception('Invalid extension of an asset: {0}. Only images are allowed'.format(
                asset_source_decoded_url)
            )

        try:
            social_token = SocialToken.objects.get(account=social_account_id)
        except SocialToken.DoesNotExist:
            raise ObjectDoesNotExist('Social Account of the user seems to be disconnected or removed.')

        # Type of media path has to be 'http'
        media_path_type = upload_api_service['media_path_type']
        if media_path_type == 'http':
            log_message = "[{0}] Uploading...".format(get_current_formatted_datetime())
            '''helper.update_asset_status(
                upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADING.value, log_message
            )'''
            asset_status.append('uploading')
            log_messages.append(log_message)

            access_token = social_token.token
            # token_expiry = social_token.expires_at

            headers = {
                'Authorization': 'Bearer ' + access_token,
                "Content-Type": "application/json",
                "x-li-format": "json"
            }

            data = {
                "comment": asset_description,
                "content": {
                    "title": asset_title,
                    "submitted-url": asset_source_decoded_url,
                    "submitted-image-url": asset_source_decoded_url
                },
                "visibility": {
                    "code": "connections-only"
                }
            }

            response = requests.post(LINKED_IN_API_URL, headers=headers, data=json.dumps(data))
            uploaded_media_id = ''
            if response.status_code == 201:
                log_message = '[{0}] Uploaded successfully'.format(get_current_formatted_datetime())
                uploaded_media_id = response.json()['updateKey']
                '''upload_status = UploadAssetDetail.UploadStatus.UPLOADED.value'''
                asset_status.append('uploaded')
                log_messages.append(log_message)
            elif response.status_code == 400:
                log_message = '[Warning][{0}] {1}'.format(get_current_formatted_datetime(), response.json()['message'])
                '''upload_status = UploadAssetDetail.UploadStatus.INVALID.value'''
                asset_status.append('invalid')
                log_messages.append(log_message)
                logger.debug('{0}. Asset: {1}'.format(response.json()['message'], asset_source_decoded_url))
            else:
                # Failed to post on LinkedIn
                log_message = '[Error][{0}] {1}'.format(get_current_formatted_datetime(), response.json()['message'])
                '''upload_status = UploadAssetDetail.UploadStatus.INVALID.value'''
                log_messages.append(log_message)

            '''helper.update_asset_status(
                upload_asset_detail_id, upload_status, log_message, uploaded_media_id
            )'''
            return [upload_asset_detail_id, asset_status, log_messages]
        else:
            raise Exception("Social Platform 'LinkedIn' is strictly configured for accepting only 'http' assets.")
    except Exception as ex:
        log_message = "[Severe] [{0}] {1}".format(get_current_formatted_datetime(), ex)
        if upload_asset_detail_id is not None:
            '''helper.update_asset_status(
                upload_asset_detail_id, UploadAssetDetail.UploadStatus.INVALID.value, log_message
            )'''
            logger.exception(ex)
            log_messages.append(log_message)
            asset_status.append('invalid')
            return [upload_asset_detail_id, asset_status, log_messages]


