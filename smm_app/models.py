from __future__ import unicode_literals
from allauth.socialaccount.models import SocialAccount
from smm_app import settings
from datetime import datetime
import os
from django.urls import reverse
from django.db import models
from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth.models import User
from django.utils import timezone
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

import urlparse
from django.core.exceptions import ValidationError
from enum import Enum
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
#from emam_middleware import settings


def download_directory_path(metadata_asset_id, for_upload_asset=False):
    """
    :param int metadata_asset_id: metadata-id or upload-asset-detail-id
    :param bool for_upload_asset:
    :return: (string) file-path
    """
    if for_upload_asset is False:
        directory_prefix = settings.METADATA_DIRECTORY_PREFIX
    else:
        directory_prefix = settings.ASSET_DIRECTORY_PREFIX

    # Path will be {settings.MEDIA_DOWNLOADS}/directory_prefix<metadata_asset_id> with 0755 permission
    asset_dir = os.path.join(settings.MEDIA_DOWNLOADS, directory_prefix + '{0}'.format(metadata_asset_id))
    if not os.path.isdir(asset_dir):
        try:
            os.makedirs(asset_dir, 0755)
        except OSError:
            pass

    return asset_dir



def get_current_formatted_datetime():
    return datetime.now().strftime(settings.DATETIME_DISPLAY_FORMAT)



class MetaData(models.Model):
    """
    >> Two fields 'emam_user' and 'post_to_social_media' in this model

    Development iteration period.
    """
    class DownloadStatus(Enum):
        NA = 'n/a'
        PENDING = 'pending'
        QUEUED = 'queued'
        DOWNLOADING = 'downloading'
        DOWNLOADED = 'downloaded'
        ERROR = 'error'

    title = models.CharField(max_length=254)
    file_name = models.CharField(max_length=254, blank=True, default='')
    file_size = models.CharField(max_length=20, blank=True, default='')
    file_description = models.TextField(blank=True, default='')
    asset_id = models.CharField(max_length=100, blank=True, default='')
    download_status = models.CharField(
        max_length=20, choices=((x.value, x.name.title()) for x in DownloadStatus), default=DownloadStatus.NA.value
    )

    # Receive when any eMAM user posting asset to social platform
    emam_user = models.PositiveIntegerField(blank=True, null=True)

    # Social platform(s) (comma separated - if multiple) on which asset needs to be uploaded
    social_platform = models.CharField(max_length=150, blank=True, default='')

    # Receive when eMAM wants to publish asset on Client Server (For BrightCove client only)
    client_account_id = models.CharField(max_length=100, blank=True, default='')
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def save(self, *args, **kwargs):
        # On save, add current date on created_at
        if not self.id:
            self.created_at = timezone.now()
        return super(MetaData, self).save(*args, **kwargs)

    def __str__(self):
        return self.title + '_' + self.asset_id

    class Meta(object):
        verbose_name = 'Metadata'
        verbose_name_plural = 'Metadata'



class AssetDetail(models.Model):
    """Development iteration period."""
    class DownloadStatus(Enum):
        NA = 'n/a'
        PENDING = 'pending'
        QUEUED = 'queued'
        DOWNLOADING = 'downloading'
        DOWNLOADED = 'downloaded'
        ERROR = 'error'
        INVALID = 'invalid'

    class AssetType(Enum):
        HIGH_RESOLUTION = 'high_resolution'
        LOW_RESOLUTION = 'low_resolution'
        THUMBNAIL = 'thumbnail'
        HIGH_RESOLUTION_SOURCE_FILE = 'high_resolution_source_file'
        LOW_RESOLUTION_SOURCE_FILE = 'low_resolution_source_file'

    metadata = models.ForeignKey(MetaData, on_delete=models.CASCADE)
    asset_type = models.CharField(max_length=50, choices=((x.value, x.name.title()) for x in AssetType))
    asset_source_url = models.CharField(max_length=254)
    download_status = models.CharField(
        max_length=20, choices=((x.value, x.name.title()) for x in DownloadStatus), default=DownloadStatus.NA.value
    )
    stored_filename = models.CharField(max_length=50, blank=True, default='')

    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def save(self, *args, **kwargs):
        # On save, add current date on created_at
        if not self.id:
            self.created_at = timezone.now()

        return super(AssetDetail, self).save(*args, **kwargs)

    def __str__(self):
        return self.asset_source_unquoted_url

    def type(self):
        return self.get_asset_type_display().replace('_', ' ')

    @property
    def asset_source_unquoted_url(self):
        return urlparse.unquote(self.asset_source_url)

    @property
    def asset_source_decoded_url(self):
        return self.asset_source_unquoted_url.replace(" ", "%20")



class UploadAssetDetail(models.Model):
    """Development iteration period."""
    class UploadTo(Enum):
        # NOTE: `CLIENT` value is mapped with emam.settings.UPLOAD_API_SERVICE's key `client`
        CLIENT = 'client'

    class UploadStatus(Enum):
        NA = 'n/a'
        PENDING = 'pending'
        QUEUED = 'queued'
        UPLOADING = 'uploading'
        UPLOADED = 'uploaded'
        REJECTED = 'rejected'
        DELETED = 'deleted'
        ERROR = 'error'
        INVALID = 'invalid'
        PREPARING = 'preparing'

    class UploadType(Enum):
        CLIENT = 'client'
        SOCIAL = 'social_media'
        OTHER = 'other'

    asset_detail = models.ForeignKey(AssetDetail, on_delete=models.CASCADE)
    upload_to = models.CharField(max_length=253, default=UploadTo.CLIENT.value)
    upload_type = models.CharField(max_length=20, default=UploadType.CLIENT.value)
    upload_to_account = models.CharField(max_length=254, default='', blank=True)
    social_account_id = models.PositiveIntegerField(blank=True, null=True)

    # For Getty, submission-type like 'getty_editorial_still', 'getty_creative_still', 'getty_editorial_video' etc..
    media_type = models.CharField(max_length=50, blank=True, default='')

    message = models.TextField(blank=True, default='')
    upload_status = models.CharField(
        max_length=20,
        choices=((x.value, x.name.title()) for x in UploadStatus),
        blank=True,
        default=UploadStatus.PENDING.value,
    )
    uploaded_media_id = models.CharField(max_length=254, blank=True, default='')
    uploaded_at = models.DateTimeField(null=True, blank=True, editable=False)
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    @property
    def requested_by(self):
        return User.objects.get(id=int(self.asset_detail.metadata.emam_user)).username

    def save(self, *args, **kwargs):
        # On save, add current date on created_at
        if not self.id:
            self.created_at = timezone.now()
        return super(UploadAssetDetail, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.message

    class Meta(object):
        verbose_name = 'Upload Asset Detail'
        verbose_name_plural = 'Upload Asset Detail'







class SocialAccountMembers(models.Model):
    social_account = models.ForeignKey(SocialAccount, on_delete=models.CASCADE, null=True)
    #getty_account = models.ForeignKey(GettyTokens, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    is_active = models.BooleanField(default=1)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    updated_at = models.DateTimeField(null=True, blank=True, editable=False)
    user_username = models.TextField(null=True)

class AppAccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return False

class AppSocialAccountAdapter(DefaultSocialAccountAdapter):
    def get_connect_redirect_url(self, request, socialaccount):
        return reverse('home')



def download_directory_path(metadata_asset_id, for_upload_asset=False):
    """
    :param int metadata_asset_id: metadata-id or upload-asset-detail-id
    :param bool for_upload_asset:
    :return: (string) file-path
    """
    if for_upload_asset is False:
        directory_prefix = settings.METADATA_DIRECTORY_PREFIX
    else:
        directory_prefix = settings.ASSET_DIRECTORY_PREFIX

    # Path will be {settings.MEDIA_DOWNLOADS}/directory_prefix<metadata_asset_id> with 0755 permission
    asset_dir = os.path.join(settings.MEDIA_DOWNLOADS, directory_prefix + '{0}'.format(metadata_asset_id))
    if not os.path.isdir(asset_dir):
        try:
            os.makedirs(asset_dir, 0755)
        except OSError:
            pass

    return asset_dir

def get_current_formatted_datetime():
    return datetime.now().strftime(settings.DATETIME_DISPLAY_FORMAT)

