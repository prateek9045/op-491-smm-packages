from django import template
import logging


register = template.Library()
logger = logging.getLogger(__name__)

@register.assignment_tag
def member_of_accounts(user_id):
    try:
        from emam.models import SocialAccountMembers
        accounts = SocialAccountMembers.objects.filter(user_id=user_id, is_active=1)
        data =[]
        for account in accounts:
            data.append(account)
        return data
    except Exception as ex:
        logger.error(ex)