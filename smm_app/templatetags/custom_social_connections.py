import ast

from django import template

import logging
register = template.Library()
logger = logging.getLogger(__name__)

@register.assignment_tag
def getty_user_accounts(user_id):
    try:
        from emam.models import GettyTokens
        getty_tokens = GettyTokens.objects.filter(user_id=user_id)
        data = []
        for getty_token in getty_tokens:
            extra_data = ast.literal_eval(getty_token.extra_data)
            account_name = extra_data['name'] + '(' + extra_data['email'] + ')'
            data.append({'account_name': account_name, 'id': getty_token.id})

        return data
    except Exception as ex:
        logger.error(ex)
