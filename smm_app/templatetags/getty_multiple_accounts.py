from django import template
from django.db.models import Q
import logging

import ast

register = template.Library()
logger = logging.getLogger(__name__)

@register.assignment_tag
def member_of_accounts(user_id):
    try:
        from emam.models import SocialAccountMembers
        accounts = SocialAccountMembers.objects.filter(user_id=user_id, is_active=1)
        data =[]
        data_dict = []
        for account in accounts:
            if account.getty_account:
                data.append(account.getty_account.extra_data)
                for x in data:
                    data_dict.append(ast.literal_eval(x))
                logger.error(data_dict[0]['email'])
        return data_dict[0]['email']
    except Exception as ex:
        logger.error(ex)