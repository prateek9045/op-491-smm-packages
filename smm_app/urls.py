from importlib import import_module
from django.contrib import admin
from django.conf.urls import include, url
from allauth.socialaccount.views import ConnectionsView
import smm_app.views
from . import views
from django.contrib.auth.decorators import login_required
from rest_framework.authtoken.views import obtain_auth_token
from allauth.socialaccount import providers
from django.conf import settings
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)
from rest_framework.routers import DefaultRouter



router = DefaultRouter()
router.register(r'metadata', views.MetaDataViewSet)
router.register(r'users', views.UserViewSet)


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', smm_app.views.index, name='index'),
    url(r'^logout/$', smm_app.views.logout_user, name='logout'),
    url(r'^account/login/', smm_app.views.user_login, name='login'),
    url(r'^account/social/connections/$', login_required(ConnectionsView.as_view()), name='home'),
    url(r'^account/social/login/error/$', smm_app.views.social_login_error),
    url(r'^account/social/login/cancelled/$', smm_app.views.social_login_cancelled),
    url(r'^account/', include('allauth.urls')),
    url(r'^account/fb-posts', smm_app.views.social_media_share, name='social_media_share'),
    url(r'^account/social/connections/socialaccount-members/edit_user', smm_app.views.edit_social_users_status, name='edit_user'),
    url(r'^account/social/connections/socialaccount-members/remove', smm_app.views.remove_linked_user, name='remove_user'),
    url(r'^account/social/connections/socialaccount-members/add', smm_app.views.add_new_members, name='add_new_members'),
    url(r'^account/social/connections/socialaccount-members', smm_app.views.social_account_multiple_users, name='social_account_multiple_users'),
    url(r'^account/get-fb-modal', smm_app.views.get_fb_modal, name='fb_modal'),
    url(r'^account/get-fb-pages', smm_app.views.get_fb_pages, name='fb_user_pages'),
    url(r'^account/post-to-fb', smm_app.views.post_to_fb, name='post_to_fb'),
    url(r'^get-upload-notify$', smm_app.views.GetUploadNotifyView.as_view(), name='get-upload-notify'),
    url(r'^api/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^api/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
    url(r'^api/token/verify/$', TokenVerifyView.as_view(), name='token_verify'),
    url(r'^api/token/', obtain_auth_token, name='api-token'),
    url(r'^api/', include(router.urls)),
]
