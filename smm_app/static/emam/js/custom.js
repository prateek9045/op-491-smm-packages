$(function(){
    $('body').on('click', '.popup-modal', function(event) { //due to ajax data request with datatable grids

        //path of the route
        var route = $(this).data("route");
        // name of the form that we have to edit
        var name = $(this).data("name");

        if(!$(this).data("name")) {
            alert('Error: name attribute not defined. Please define name attribute.');
            return false;
        }

        if(!$(this).data("route")) {
            alert('Error: route attribute not defined. Please define route attribute.');
            return false;
        }

        $.ajax({
            type: "GET",
            url : route,
            async : true,
            beforeSend: function() {
                $('#modal').modal('show');
                $("#modal").find("#formTitle").html(name);
                setTimeout(function () {
                    //$('#loader').html() defined in layout before editing modal
                    $("#modal").find("#datatResult").html($('#loader').html());
                } , 10);
            },
            success:function(data, textStatus, jqXHR)
            {
                $("#modal").find("#datatResult").html(data);
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                $('#modal').modal('hide');
                alert('You have '+errorThrown+' your required request..');
            }
        });
        return false;
        event.preventDefault(); //STOP default action
        event.unbind(); //unbind. to stop multiple form submit.
    })
})