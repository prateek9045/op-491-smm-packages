"""
Django settings for emam_middleware project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

# Middleware Root URL to receive upload status (change it if required)
UPLOAD_NOTIFY_CALLBACK_URL = 'http://13.92.176.35:81'

#BASE_DIR = os.path.dirname(os.path.dirname(__file__))
#MEDIA_DOWNLOADS = os.path.join(BASE_DIR, 'static', 'emam_assets')

DOWNLOAD_MAX_RETRIES = 2
DOWNLOAD_NEXT_RETRY_IN = 5
DATETIME_DISPLAY_FORMAT = "%Y-%m-%d %H:%M:%S"

METADATA_DIRECTORY_PREFIX = 'meta_'
ASSET_DIRECTORY_PREFIX = 'asset_'

UPLOAD_API_SERVICE = {
    'youtube': {
        'service_name': 'youtube',
        'api_version': 'v3',
        'upload_handler': 'google',
        'provider': 'google',
        'email_variable': 'email',
        'media_path_type': 'http',
        'allowed_media': 'video'
    },
    'google_plus': {
        'service_name': 'plusDomains',
        'api_version': 'v1',
        'upload_handler': 'google',
        'provider': 'google',
        'email_variable': 'email',
        'media_path_type': 'http'
    },
    'linked_in': {
        'service_name': '',
        'api_version': '',
        'upload_handler': 'linked_in',
        'provider': 'linkedin_oauth2',
        'email_variable': 'emailAddress',
        'media_path_type': 'http',  # DO NOT CHANGE IT for linked_in here
        'allowed_media': 'image'
    },
    'twitter': {
        'upload_handler': 'twiter',
        'provider': 'twitter',
        'media_path_type': 'http'
    },
    'facebook': {
        'upload_handler': 'facebook',
        'provider': 'facebook',
        'email_variable': 'email',
        'media_path_type': 'http'
    }
}


REST_FRAMEWORK = {


    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (

        'rest_framework_simplejwt.authentication.JWTAuthentication',
    )

}