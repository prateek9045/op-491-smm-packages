
#First check "allauth" module is installed or not. If not run the following command to install:-
	> pip install django-allauth==0.32.0

# To install the "smm_app" module run the following command:
	
	1. > pip install --user path/to/.tar_file
		example:
			> pip install --user E:/smm_app/dist/django-smm-0.1.tar.gz

	2. python manage.py migrate smm_app

# After installation completes add following in your "settings.py" file of your core app.
	
	1. from smm_app.settings import UPLOAD_API_SERVICE
	2.  INSTALLED_APPS = (
						...........
						...........,
						
						'smm_app',
						
						...........,
						...........
					)
					

# To uninstall run the following command:
	> pip uninstall django-smm
					
# If there is any change in the code, then run the follwing command to reflect to the changes in .tar file.
	
	> python setup.py sdist (run it from inside smm_app directory)