from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from smm_app.emam_gateway import EMAMGateway


class LoginForm(forms.Form):
    username = forms.CharField(max_length=255, required=True)
    password = forms.CharField(widget=forms.PasswordInput, required=True)

    def clean(self):
        """
        Custom handling of user login form
        Added registration of user (only if eMAM user logging in first time) and `is_active_user` check

        :return:
        """
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        #Restricting super-user to login to front-end panel.
        superusers_usernames = User.objects.filter(is_superuser=True).values_list('username')
        for superusers_username in superusers_usernames:
            if str(superusers_username[0]) == str(username):
                raise forms.ValidationError(
                    "Your account doesn't have access to this panel.")

        user = authenticate(username=username, password=password)
        if not user:
            """ If user does not exist in Database

            -- eMAM User Authentication and Registration --

            When user does not exist or may be the eMAM user has changed password in eMAM server
            Now, authenticate user from eMAM Authentication API and register with
            specified credentials if authenticated, in middleware application
            """

            ''' Custom way to authenticate
            data = settings.EMAM_USER_AUTH_API_BODY
            data['username'] = username
            data['password'] = password

            headers = settings.EMAM_USER_AUTH_API_HEADERS

            is_authenticated = requests.post(settings.EMAM_USER_AUTH_API, data=data, headers=headers)
            '''

            emam_gateway = EMAMGateway()
            is_authenticated = emam_gateway.authenticate_user(username, password, 0, settings.EMAM_LICENSE_KEY)
            if is_authenticated:
                """
                Check if eMAM user does already exist in middleware but password is incorrect.
                Means, user might have changed password in eMAM Server but middleware app is not aware about it
                In this case, we will change only password by username if exists and proceed for login
                """
                try:
                    user = User.objects.get(username=username)
                    user.set_password(password)
                    user.save()
                except User.DoesNotExist:
                    # Register eMAM user
                    user = User.objects.create_user(username=username, password=password)
                except MultipleObjectsReturned:
                    raise forms.ValidationError("Internal Server error. Please contact your application administrator")
            else:
                raise forms.ValidationError("Invalid login! Please try again.")

        if not user.is_active:
            raise forms.ValidationError("Your account has been disabled. Please contact your application administrator")

        return self.cleaned_data

    def login(self):
        """
        Proceed for login if user login inputs are valid

        :return:
        """
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)

        return user