from __future__ import unicode_literals

import httplib
import random
import time
import logging
import os

import httplib2
from allauth.socialaccount.models import *
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from oauth2client.client import OAuth2Credentials

#from emam.models import *
#from emam.scheduler import helper
from smm_app import settings
from smm_app.models import download_directory_path
from smm_app.service import get_current_formatted_datetime, download_asset_to_upload
from django.core.exceptions import ObjectDoesNotExist

asset_status = []
response_id = []
log_messages = []
# Get an instance of a logger
logger = logging.getLogger(__name__)

# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.
httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 5

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (
    httplib2.HttpLib2Error, IOError, httplib.NotConnected,
    httplib.IncompleteRead, httplib.ImproperConnectionState,
    httplib.CannotSendRequest, httplib.CannotSendHeader,
    httplib.ResponseNotReady, httplib.BadStatusLine
)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]
GOOGLE_TOKEN_URI = "https://accounts.google.com/o/oauth2/token"


def resumable_upload(upload_asset_detail_id, request):
    """
    :param int upload_asset_detail_id:
    :param request:
    :param string method:
    :return:
    """
    response = None
    error = None
    retry = 0

    while response is None:
        try:
            logger.info(response)
            log_message = "\n[{0}] Uploading...".format(get_current_formatted_datetime())
            '''helper.update_asset_status(
                upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADING.value, log_message
            )'''
            asset_status.append('uploading')
            log_messages.append(log_message)

            status, response = request.next_chunk()

            if response:
                # Received upload response
                if 'id' in response:
                    log_message = '[{0}] Media id "{1}" was successfully uploaded.'.format(
                        get_current_formatted_datetime(), response['id']
                    )
                    log_messages.append(log_message)

                    response_id.append(response['id'])
                    '''helper.update_asset_status(
                        upload_asset_detail_id, uploaded_media_id=response['id'], log_message=log_message
                    )'''
                    return response
                else:
                    log_message = '[Severe] [{0}] The upload failed with response: {1}.'.format(
                        get_current_formatted_datetime(), response
                    )
                    log_messages.append(log_message)
                    '''helper.update_asset_status(
                        upload_asset_detail_id, UploadAssetDetail.UploadStatus.INVALID.value, log_message
                    )'''
                    asset_status.append('invalid')
        except HttpError, e:
            if e.resp.status in RETRIABLE_STATUS_CODES:
                error = "HTTP error {0} occurred:\n{1}.".format(e.resp.status, e.content)
            else:
                raise
        except RETRIABLE_EXCEPTIONS, e:
            error = "HTTP error {0} occurred:\n{1}.".format(e.resp.status, e.content)
        if error is not None:
            retry += 1
            if retry > MAX_RETRIES:
                log_message = "[Error] [{0}] {1} \nNo longer attempting to retry. Uploading failed.".format(
                    get_current_formatted_datetime(), error
                )
                '''helper.update_asset_status(
                    upload_asset_detail_id, UploadAssetDetail.UploadStatus.ERROR.value, log_message
                )'''
                log_messages.append(log_message)
                asset_status.append('error')
                break

            max_sleep = 2 ** retry
            sleep_seconds = random.random() * max_sleep

            log_message = "[Error] [{0}] {1} \nRetrying in {2} seconds".format(
                get_current_formatted_datetime(), error, sleep_seconds
            )
            log_messages.append(log_message)
            #helper.update_upload_log(upload_asset_detail_id, log_message)

            time.sleep(sleep_seconds)


def build_resource(properties):
    """
    :param dict properties:
    :return:
    """
    resource = {}
    for p in properties:
        # Given a key like "snippet.title", split into "snippet" and "title", where
        # "snippet" will be an object and "title" will be a property in that object.
        prop_array = p.split('.')
        ref = resource
        for pa in range(0, len(prop_array)):
            is_array = False
            key = prop_array[pa]
            # Convert a name like "snippet.tags[]" to snippet.tags, but handle
            # the value as an array.
            if key[-2:] == '[]':
                key = key[0:len(key)-2:]
                is_array = True
            if pa == (len(prop_array) - 1):
                # Leave properties without values out of inserted resource.
                if properties[p]:
                    if is_array:
                        ref[key] = properties[p].split(',')
                    else:
                        ref[key] = properties[p]
            elif key not in ref:
                # For example, the property is "snippet.title", but the resource does
                # not yet have a "snippet" object. Create the snippet object here.
                # Setting "ref = ref[key]" means that in the next time through the
                # "for pa in range ..." loop, we will be setting a property in the
                # resource's "snippet" object.
                ref[key] = {}
                ref = ref[key]
            else:
                # For example, the property is "snippet.description", and the resource
                # already has a "snippet" object.
                ref = ref[key]

    return resource


def remove_empty_kwargs(**kwargs):
    good_kwargs = {}
    if kwargs is not None:
        for key, value in kwargs.iteritems():
            if value:
                good_kwargs[key] = value

    return good_kwargs


def google_plus_video(api_service, api_version, upload_asset_detail_id, credentials, media_file, title, description):
    """
    Initiate user upload request

    :param string api_service:
    :param string api_version:
    :param int upload_asset_detail_id:
    :param OAuth2Credentials credentials:
    :param string media_file:
    :param string title:
    :param string description: Description of posting activity
    :return: media_id|None
    """
    service = build(api_service, api_version, http=credentials.authorize(httplib2.Http()))

    request = service.media().insert(
        media_body=MediaFileUpload(media_file, chunksize=-1, resumable=True),
        body={'displayName': title},
        userId='me',
        collection='cloud',
    )
    upload_response = resumable_upload(upload_asset_detail_id, request)

    media_id = None
    if upload_response is not None:
        media_id = upload_response['id']

        logger.debug('Media ID: {0}.'.format(media_id))

        """
        "objectType": "A String", # The type of media object. Possible values include,
        but are not limited to, the following values:
              # - "photo" - A photo.
              # - "album" - A photo album.
              # - "video" - A video.
              # - "article" - An article, specified by a link.
        """

        if 'videoStatus' in upload_response:
            object_type = 'video'
        else:
            object_type = 'photo'

        log_message = '[{0}] Creating Activity with uploaded media file..'.format(get_current_formatted_datetime())
        #helper.update_upload_log(upload_asset_detail_id, log_message)

        activity_response = service.activities().insert(
            userId='me',
            body={
                'object': {
                    'content': title + "\n" + description,
                    'attachments': [{
                        'objectType': object_type,
                        'id': media_id
                    }]
                },
                'access': {
                    'items': [{
                        'type': 'domain'
                    }],
                    'domainRestricted': True
                }
            }
        ).execute()

        logger.info('Post Insert Logs {0}.'.format(request))

        if activity_response and 'id' in activity_response:
            log_message = '[{0}] Activity was posted successfully with attached media file. Activity id: {1}'.format(
                get_current_formatted_datetime(), activity_response['id']
            )
            #upload_status = UploadAssetDetail.UploadStatus.UPLOADED.value
            log_messages.append(log_message)
            asset_status.append('uploaded')
        else:
            log_message = '[Severe] [{0}] Error while creating Activity.'.format(
                get_current_formatted_datetime()
            )
            #upload_status = UploadAssetDetail.UploadStatus.INVALID.value
            log_messages.append(log_message)
            asset_status.append('invalid')

        #helper.update_asset_status(upload_asset_detail_id, upload_status, log_message)

    return media_id


def videos_insert(
    api_service, api_version, upload_asset_detail_id,
    credentials, properties, media_file, **kwargs
):
    """
    Initiate user upload request for youtube

    :param string api_service:
    :param string api_version:
    :param int upload_asset_detail_id:
    :param OAuth2Credentials credentials:
    :param dict properties:
    :param string media_file:
    :param kwargs:
    :return: video_id|None
    """

    service = build(api_service, api_version, http=credentials.authorize(httplib2.Http()))
    resource = build_resource(properties)
    kwargs = remove_empty_kwargs(**kwargs)

    request = service.videos().insert(
        body=resource,
        media_body=MediaFileUpload(media_file, chunksize=-1, resumable=True),
        **kwargs
    )

    video_id = None
    response = resumable_upload(upload_asset_detail_id, request)
    if response is not None:
        video_id = response['id']
        log_message = '[{0}] Video uploaded successfully'.format(get_current_formatted_datetime())
        #helper.update_asset_status(upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADED.value, log_message)
        log_messages.append(log_message)
        asset_status.append('uploaded')
    return video_id


def uploader(asset):
    """
    Initialize Upload video request on youtube or google_plus

    :param string asset:
    :return:
    """

    upload_asset_detail_id = None
    try:
        asset_data = asset.split("|")
        asset_title = asset_data[0]
        asset_source_decoded_url = asset_data[1]
        upload_asset_detail_id = int(asset_data[2])
        upload_to = asset_data[5]
        social_account_id = asset_data[6]
        asset_detail_id = int(asset_data[7])
        asset_description = asset_data[9]
        asset_source_unquoted_url = asset_data[10]
        upload_api_service = settings.UPLOAD_API_SERVICE[upload_to]
        api_service = upload_api_service['service_name']
        api_version = upload_api_service['api_version']
        service_provider = upload_api_service['provider']

        # Type of media path 'local' or 'http'
        media_path_type = upload_api_service['media_path_type']

        if media_path_type == 'local':
            media_file = asset_source_unquoted_url
        else:
            media_file = os.path.join(
               download_directory_path(asset_detail_id, True),
               asset_source_decoded_url.split("/")[-1]
            )

            if not os.path.isfile(media_file):
                log_message = '[{0}] Downloading asset..'.format(get_current_formatted_datetime())
                #helper.update_asset_status(upload_asset_detail_id, UploadAssetDetail.UploadStatus.QUEUED.value, log_message)
                log_messages.append(log_message)
                asset_status.append('queued')
                # Download media asset from provided asset url link
                media_file = download_asset_to_upload(
                    asset_detail_id, upload_asset_detail_id, asset_source_decoded_url
                )

        # media_file will be None if downloading is failed or wrong file path
        if media_file is not None:
            social_app = SocialApp.objects.get(provider=service_provider)

            try:
                social_token = SocialToken.objects.get(account=social_account_id)
            except SocialToken.DoesNotExist:
                raise ObjectDoesNotExist('Social Account of the user seems to be disconnected or removed.')

            access_token = social_token.token
            refresh_token = social_token.token_secret
            token_expiry = social_token.expires_at
            credentials = OAuth2Credentials(
                access_token, social_app.client_id, social_app.secret, refresh_token,
                token_expiry, GOOGLE_TOKEN_URI, 'SurmountSoft'
            )

            if upload_to == 'youtube':
                media_id = videos_insert(api_service, api_version, upload_asset_detail_id, credentials, {
                        'snippet.categoryId': '22',
                        'snippet.defaultLanguage': '',
                        'snippet.description': asset_description,
                        'snippet.tags[]': '',
                        'snippet.title': asset_title,
                        'status.embeddable': '',
                        'status.license': '',
                        'status.privacyStatus': 'public',
                        'status.publicStatsViewable': ''
                    },
                    media_file,
                    part='snippet,status'
                )
            else:
                media_id = google_plus_video(
                    api_service, api_version, upload_asset_detail_id,
                    credentials, media_file, asset_title, asset_description
                )

            # if media_path_type is 'http' then remove downloaded file after successful upload
            if media_id is not None and media_path_type == 'http':
                #helper.silent_remove(media_file)
                return [upload_asset_detail_id, asset_status, log_messages, media_file, response_id]
        return [upload_asset_detail_id, asset_status, log_messages, False, response_id]
    except Exception as ex:
        log_message = "[Severe] [{0}] {1}".format(get_current_formatted_datetime(), ex)
        if upload_asset_detail_id is not None:
            '''helper.update_asset_status(
                upload_asset_detail_id, UploadAssetDetail.UploadStatus.INVALID.value, log_message
            )'''
            log_messages.append(log_message)
            asset_status.append('invalid')
            return [upload_asset_detail_id, asset_status, log_messages]

        logger.exception(ex)

