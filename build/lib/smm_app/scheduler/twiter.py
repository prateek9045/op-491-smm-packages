from allauth.socialaccount.models import SocialToken, SocialApp
from django.core.exceptions import ObjectDoesNotExist
from twitter.api import *
#from emam.models import *
#from emam.scheduler import helper
import logging
from smm_app import settings
# Get an instance of a logger
from smm_app.models import get_current_formatted_datetime
from smm_app.service import download_asset_to_upload

logger = logging.getLogger(__name__)
asset_status = []
log_messages = []

def uploader(asset):
    """
    Initialize Upload video request

    :param string asset:
    :return:
    """
    upload_asset_detail_id = None
    try:
        asset_data = asset.split("|")
        asset_title = asset_data[0]
        asset_source_decoded_url = asset_data[1]
        upload_asset_detail_id = int(asset_data[2])
        upload_to = asset_data[5]
        social_account_id = asset_data[6]
        asset_detail_id = int(asset_data[7])
        asset_source_unquoted_url = asset_data[10]

        upload_api_service = settings.UPLOAD_API_SERVICE[upload_to]
        service_provider = upload_api_service['provider']

        try:
            social_token = SocialToken.objects.get(account=social_account_id)
        except SocialToken.DoesNotExist:
            raise ObjectDoesNotExist('Social Account of the user seems to be disconnected or removed.')

        # Type of media path 'local' or 'http'
        media_path_type = upload_api_service['media_path_type']
        if media_path_type == 'local':
            media_file = asset_source_unquoted_url
        else:
            media_file = os.path.join(

               asset_source_decoded_url.split("/")[-1]
            )

            if not os.path.isfile(media_file):
                log_message = '[{0}] Downloading asset..'.format(get_current_formatted_datetime())
                #helper.update_asset_status(
                #    upload_asset_detail_id, UploadAssetDetail.UploadStatus.QUEUED.value, log_message
                #)
                asset_status.append('queued')
                log_messages.append(log_message)
                # Download media asset from provided asset url link
                media_file = download_asset_to_upload(
                    asset_detail_id, upload_asset_detail_id, asset_source_decoded_url
                )

        # media_file will be None if downloading is failed or wrong file path
        if media_file is not None:
            print 'uploading..............'
            # Uploading..
            log_message = "\n[{0}] Uploading...".format(get_current_formatted_datetime())
            #helper.update_asset_status(
            #   upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADING.value, log_message
            #)
            asset_status.append('uploading')
            log_messages.append(log_message)
            print asset_status
            print media_file

            access_token = social_token.token
            refresh_token = social_token.token_secret
            social_app = SocialApp.objects.get(provider=service_provider)
            print 'check..............80'
            api = Api(
                consumer_key=social_app.client_id,
                consumer_secret=social_app.secret,
                access_token_key=access_token,
                access_token_secret=refresh_token,
                debugHTTP=True,
            )
            response = api.PostUpdate(status=asset_title, media=media_file).AsDict()
            if 'id_str' in response:
                log_message = '[{0}] Uploaded successfully'.format(get_current_formatted_datetime())
                #helper.update_asset_status(
                #    upload_asset_detail_id, UploadAssetDetail.UploadStatus.UPLOADED.value,
                #    log_message, response['id_str']
                #)
                asset_status.append('uploaded')
                print asset_status
                log_messages.append(log_message)
                # if media_path_type is 'http' then remove downloaded file after successful upload
                if response['id_str'] is not None and media_path_type == 'http':
                    print 'silent remove'
                    #helper.silent_remove(media_file)
                    return [upload_asset_detail_id,asset_status,log_messages, media_file]
        return [upload_asset_detail_id, asset_status, log_messages]
    except Exception as ex:
        print ex
        log_message = "[Severe] [{0}] {1}".format(get_current_formatted_datetime(), ex)
        log_messages.append(log_message)
        if upload_asset_detail_id is not None:
            '''helper.update_asset_status(
                upload_asset_detail_id, UploadAssetDetail.UploadStatus.INVALID.value, log_message
            )'''
            return [upload_asset_detail_id, asset_status, log_messages]
        #logger.exception(ex)
        print log_message