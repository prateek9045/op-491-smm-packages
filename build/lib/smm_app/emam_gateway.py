from django.conf import settings
from suds.client import Client
from suds.xsd.doctor import Import, ImportDoctor
from suds.sudsobject import asdict


class EMAMGateway:
    upload_id = ''

    def __init__(self, url='', name_space='http://tempuri.org/'):
        """Initialize the eMAM Gateway.
        :param url: Gateway URL
        :param name_space: NameSpace
        """
        self.url = url or settings.EMAM_GATEWAY_WSDL
        self.name_space = name_space
        imp = Import('http://www.w3.org/2001/XMLSchema', location='http://www.w3.org/2001/XMLSchema.xsd')
        imp.filter.add(name_space)
        doctor = ImportDoctor(imp)
        self.gateway_client = Client(self.url, doctor=doctor)

    def authenticate_user(self, user_name, password, unit_id, license_key):
        """Authenticate user with eMAM Gateway.
        :param user_name: Username
        :param password: Password
        :param unit_id: Unit ID
        :param license_key: License key
        :return: True if the authentication was successful
        """
        auth_obj = self.gateway_client.factory.create('clsSoapAuthentication')
        auth_obj.UserName = user_name
        auth_obj.Password = password
        auth_obj.UnitId = unit_id
        return self.gateway_client.service.AuthenticateUser(auth_obj, license_key)

    def update_asset_metadata(self, asset_id, metadata_array):
        """Update asset custom metadata
        :param asset_id: Asset ID
        :param metadata_array: Metadata array
        :return: eMAM Gateway response, ResponseId > 0 if success
        """
        metadata_details = self.gateway_client.factory.create('MetadataConfig')
        metadata_details.AssetId = asset_id
        metadata_details.CategoryId = 0
        metadata_details.ProjectId = 0
        metadata_details.ProjectVersionId = 0
        metadata_details.Author = ''
        metadata_details.MetadataSetId = 0
        metadata_obj = self.gateway_client.factory.create('ArrayOfClsMetadataValue')
        metadata_obj.clsMetadataValue = []
        for metadata in metadata_array:
            metadata_value = self.gateway_client.factory.create('clsMetadataValue')
            metadata_value.FieldId = metadata[0]
            metadata_value.FieldValue = metadata[1]
            metadata_obj.clsMetadataValue.append(metadata_value)
        metadata_details.MetadataValues = metadata_obj

        return self.gateway_client.service.UpdateAssetMetaData(metadata_details)

    def get_custom_metadata(self, asset_id=0, unit_id=0):
        """Get custom metadata.
        :param asset_id: Asset ID, 0 to get all metadata
        :param unit_id: Unit ID, 0 to get default unit
        :return: eMAM Gateway response, ResponseId > 0 if success
        """
        return self.gateway_client.service.GetCustomMetaData(asset_id, unit_id)

    def recursive_dict(self, sudsobject):
        """ Get specific key value by iterating through the object in the form of dictionary
        returned by suds as soap XML response

        :param sudsobject:
        :return:
        """
        out = {}
        for k, v in asdict(sudsobject).iteritems():
            if hasattr(v, '__keylist__'):
                out[k] = self.recursive_dict(v)
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        data = self.recursive_dict(item)
                        try:
                            if 'FIELD_ID' in data and int(data['FIELD_ID'][0]) == settings.EMAM_UPLOAD_FIELD_ID:
                                self.upload_id = data['FIELD_VALUE'][0]
                                return data['FIELD_VALUE'][0]
                        except Exception:
                            pass
                        out[k].append(data)
                    else:
                        out[k].append(item)
            else:
                out[k] = v
        return out
