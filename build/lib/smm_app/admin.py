# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from smm_app.models import SocialAccountMembers


@admin.register(SocialAccountMembers)
class SocialAccountMemberAdmin(admin.ModelAdmin):
    list_display = ['social_account','user','user_username', 'is_active']
    readonly_fields = ('social_account', 'user','user_username','created_at', 'updated_at')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return True
