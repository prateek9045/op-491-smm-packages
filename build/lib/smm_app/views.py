from __future__ import unicode_literals

import os
import urlparse

import requests
#from allauth.socialaccount.models import SocialAccount, SocialToken
from datetime import datetime, timedelta

from allauth.socialaccount.models import SocialAccount, SocialToken
from django.conf import settings
from django.contrib import messages
from django.contrib.messages import get_messages
from django.contrib.auth import get_user_model, login
from django.db.models import Q
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_http_methods
#from models import SocialAccountMembers
from rest_framework import authentication, permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
#from  emam.scheduler.helper import exclude_added_users
#from emam.forms import LoginForm
#from .models import MetaData, UploadAssetDetail, get_current_formatted_datetime, AssetDetail, download_directory_path, \
#    SocialAccountMembers
#    GettyTokens, ClientAccounts
#from .serializers import MetaDataSerializer, UserSerializer
from rest_framework.parsers import JSONParser
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
#import emam.scheduler.helper as helper
from django.contrib.auth import logout
import facebook
import logging
import base64
from requests_toolbelt import MultipartEncoder
from django.core.exceptions import ValidationError

from smm_app.forms import LoginForm
from smm_app.models import SocialAccountMembers
from smm_app.service import restrict_superuser_to_login, exclude_added_users, guess_mime_type

User = get_user_model()

logger = logging.getLogger(__name__)


class DefaultsMixin(object):
    """
    Default settings for view authentication, permissions, filtering and pagination.
    """
    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )
    paginate_by = 25
    paginate_by_param = 'page_size'
    max_paginate_by = 100


def user_login(request):
    """
    User login for front-end SMM application

    :param request:
    :return:
    """
    try:
        next_page = request.GET.get('next')
        if request.user.is_authenticated():
            restrict_superuser_to_login(request)
            return redirect('home')
        # AuthenticationForm

        form = LoginForm(request.POST or None)

        if request.POST and form.is_valid():
            user = form.login()
            if user:
                login(request, user)
                if next_page:
                    restrict_superuser_to_login(request)
                    return HttpResponseRedirect(next_page)
                else:
                    return redirect('home')
        return render(request, 'login.html', {'form': form, 'messages': get_messages(request)})
    except Exception as ex:
        messages.error(request, 'Network Unreachable! We could not connect to eMAM Server right now. '
                                'Please check your internet connection or try again later.')
        return render(request, 'login.html', {'messages': get_messages(request)})


@login_required
def index(request):
    """
    Landing page just for redirection to home page 'social/connections'

    :param request:
    :return:
    """
    message = request.GET.get('message')
    if message:
        message_type = int(request.GET.get('message_type'))
        if message_type == 1:
            messages.info(request, message)
        else:
            messages.error(request, message)
    restrict_superuser_to_login(request)

    return redirect('home')


@login_required
def social_login_error(request):
    """
    Overrides allauth `login_error` view for custom redirection with message if social login failed

    :param request:
    :return:
    """
    messages.error(request, 'Social Login failed. Please try again.')
    return redirect('home')


@login_required
def social_login_cancelled(request):
    """
    Overrides allauth `login_cancelled` view for custom redirection with message if social login cancelled

    :param request:
    :return:
    """
    messages.error(request, 'Social Login cancelled.')
    return redirect('home')


def logout_user(request):
    logout(request)
    messages.info(request, 'Logged out successfully.')

    return redirect('login')


@login_required
def social_media_share(request):
    """
    :param request:
    :return:
    """

    upload_asset_list = UploadAssetDetail.objects.filter(
        asset_detail__metadata__emam_user=request.user.id,
        # upload_status=UploadAssetDetail.UploadStatus.PENDING.value,
        upload_to='facebook'
    ).order_by('-created_at')

    return render(request, 'shares.html', {'upload_assets': upload_asset_list})

@login_required
def social_account_multiple_users(request):
    """
    :param request:
    :return:
    """
    try:
        from smm_app.models import SocialAccountMembers
        try:
            account = ''
            account_id = request.GET.get('social_account')
            getty_account_status = request.GET.get('getty_status')
            if int(getty_account_status) == 1:
                '''account = GettyTokens.objects.filter(pk=account_id)
                linked_users = SocialAccountMembers.objects.filter(getty_account=account)'''
                logger.info('')
            else:
                account = SocialAccount.objects.filter(pk=account_id)
                linked_users = SocialAccountMembers.objects.filter(social_account=account)
            excluded_current_user_and_admin = User.objects.exclude( Q(is_superuser=True)| Q(pk=request.user.id))
            users_to_add = exclude_added_users(excluded_current_user_and_admin=excluded_current_user_and_admin, linked_user_list=linked_users)
            return render(request, 'multiple_accounts.html', {'count':len(list(linked_users)), 'linked_users': list(linked_users), 'users_to_add':users_to_add, 'social_account':account_id, 'getty_status':getty_account_status})
        except Exception as ex:
            logger.error(ex)

    except ImportError as ex:
        logger.error(ex)



@login_required
@require_http_methods(["GET"])
def remove_linked_user(request):
    """
    :param request:
    :return:
    """
    try:

        current_user = request.GET.get('social_member_id')
        social_user = SocialAccountMembers.objects.filter(pk=current_user)
        social_user.delete()
        return redirect(request.META.get('HTTP_REFERER'))
    except Exception as ex:
        logger.error(ex)

@login_required
@require_http_methods(["GET"])
def edit_social_users_status(request):
    """
    :param request:
    :return:
    """
    try:
        social_account_id = request.GET.get('social_member_id')
        social_user = SocialAccountMembers.objects.get(pk=social_account_id)
        if social_user.is_active == 1:
            social_user.is_active = 0
            social_user.save()
        else:
            social_user.is_active = 1
            social_user.save()
        return redirect(request.META.get('HTTP_REFERER'))
    except Exception as ex:
        logger.error(ex)

@login_required
@require_http_methods(["POST"])
def add_new_members(request):
    """
    :param request:
    :return:
    """
    try:
        create_user_list = []
        getty_account_check = request.GET.get('getty_status')
        account = request.GET.get('social_account')
        users_to_add = request.POST.getlist('users')
        create_new_user = request.POST.get('create-user-email')
        if create_new_user:
            create_new_user = create_new_user.split(',')
            for x in create_new_user:
                create_user_list.append(x.strip())

        if not users_to_add and not create_new_user:
            messages.error(request, 'No user is selected.')
        '''if int(getty_account_check) == 1:
            users_to_add = [int(x) for x in users_to_add]
            current_user = GettyTokens.objects.get(pk=account)
            if users_to_add:
                for x in users_to_add:
                    user_obj = User.objects.get(pk=x)
                    insert_record = SocialAccountMembers(getty_account=current_user, user=user_obj)
                    insert_record.save()
            if create_user_list:
                for x in create_user_list:
                    insert_record = SocialAccountMembers(getty_account=current_user, user_username=x)
                    insert_record.save()'''
        #else:
        users_to_add = [x for x in users_to_add]
        current_user = SocialAccount.objects.get(pk=account)
        if users_to_add:
            for x in users_to_add:
                user_obj = User.objects.get(pk=x)
                insert_record = SocialAccountMembers(social_account=current_user, user=user_obj)
                insert_record.save()
        if create_user_list:
            for x in create_user_list:
                insert_record = SocialAccountMembers(social_account=current_user, user_username=x)
                insert_record.save()


        return redirect(request.META.get('HTTP_REFERER'))
    except Exception as ex:
        logger.error(ex)

@login_required()
def get_fb_modal(request):
    """
    Get post to facebook form data to be rendered in popup modal

    :param request:
    :return:
    """

    try:
        upload_id = request.GET.get('upload_id')
        upload_api_service = settings.UPLOAD_API_SERVICE['facebook']
        social_accounts = helper.retrieve_social_member_accounts(user_id=request.user.id, upload_api_service_provider=upload_api_service['provider'])

        """social_accounts = SocialAccount.objects.filter(
            user_id=request.user.id, provider=upload_api_service['provider']
        )"""

        asset_detail = UploadAssetDetail.objects.get(
            Q(id=upload_id) &
            (Q(upload_status=UploadAssetDetail.UploadStatus.PENDING.value) |
             Q(upload_status=UploadAssetDetail.UploadStatus.ERROR.value)) &
            Q(upload_to='facebook') &
            Q(asset_detail__metadata__emam_user=request.user.id)
        ).asset_detail

        asset_file = asset_detail.asset_source_unquoted_url

        try:
            asset_thumbnail = AssetDetail.objects.get(
                metadata=asset_detail.metadata, asset_type=AssetDetail.AssetType.THUMBNAIL.value
            ).asset_source_unquoted_url
        except AssetDetail.DoesNotExist:
            asset_thumbnail = ''
        email_variable = None
        if 'email_variable' in upload_api_service:
            email_variable = upload_api_service['email_variable']

        fb_accounts = {}
        for social_account in social_accounts:
            social_identity = social_account.get_provider_account()
            if email_variable:
                extra_data = social_account.extra_data
                account_identifier = (email_variable in extra_data and str(extra_data[email_variable])) or str(extra_data['name'])
                social_identity = str(social_identity) + ' (' + account_identifier + ')'
                fb_accounts.update({social_account.id: social_identity})

        asset_mime_type, attachment_thumbnail = '', ''
        try:
            asset_mime_type = guess_mime_type(asset_file)
            if asset_mime_type == 'image' or asset_mime_type == 'video':
                attachment_thumbnail = base64.b64encode(open(asset_thumbnail, "rb").read())
        except IOError:
            attachment_thumbnail = ''

        return render(request, 'fb_modal.html', {
            'fb_accounts': fb_accounts, 'upload_id': upload_id, 'attachment_thumbnail': attachment_thumbnail,
            'asset_mime_type': asset_mime_type
        })
    except UploadAssetDetail.DoesNotExist:
        error = 'Invalid Request'
    except Exception as ex:
        error = 'Internal Server Error'
        logger.exception(ex)

    return render(request, 'fb_modal.html', {'error': error})

@login_required()
def get_fb_pages(request):
    """
    Get list of facebook pages associated with particular user account having page role as 'Admin' or 'Editor'

    :param request:
    :return:
    """
    fb_account_id = request.GET.get('social_account_id')
    try:
        social_token = SocialToken.objects.get(account=fb_account_id)
        access_token = social_token.token
        headers = {'Authorization': 'Bearer %s' % access_token, 'Content-Type': 'application/json'}
        response = requests.get('https://graph.facebook.com/me/accounts', headers=headers)
        fb_post_options = {}

        if response.status_code == 200:
            pages = response.json()
            for page in pages['data']:
                if 'EDIT_PROFILE' in page['perms']:  # check if user can publish to page
                    fb_post_options.update({page['id']: page['name']})

        return render(request, 'get_fb_post_options.html', {'fb_post_options': fb_post_options})
    except SocialToken.DoesNotExist:
        # raise ObjectDoesNotExist('Social Account of the user seems to be disconnected or removed.')
        pass

    except Exception as e:
        return render(request, 'get_fb_post_options.html', {'pages': e})

@login_required()
@csrf_protect
def post_to_fb(request):
    """
    Facebook media Post handler

    :param request:
    :return:
    """

    upload_detail_id = None
    try:
        fb_account_id = request.POST['fb_account_id']

        title = request.POST['title']
        page_id = int(request.POST['page_id'])

        upload_detail_id = request.POST['upload_detail_id']

        upload_asset_detail = UploadAssetDetail.objects.get(id=upload_detail_id)
        if upload_asset_detail.upload_status != UploadAssetDetail.UploadStatus.PENDING.value\
                and upload_asset_detail.upload_status != UploadAssetDetail.UploadStatus.ERROR.value:
            return JsonResponse({
                'status_code': 200,
                'message': 'This post has already been acknowledged. Please check the upload status.'
            })
        log_message = '[{0}] Preparing your facebook post..'.format(get_current_formatted_datetime())
        helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.PREPARING.value, log_message)

        social_token = SocialToken.objects.get(account=fb_account_id)
        access_token = social_token.token
        graph = facebook.GraphAPI(access_token)
        node_id = 'me'
        node_name = 'Own Timeline'

        """
        Fetch page access token if user request is for page
        """
        if page_id > 0:
            node_id = page_id
            resp = graph.get_object('me/accounts')
            page_access_token = None
            for page in resp['data']:
                if int(page['id']) == page_id:
                    page_access_token = page['access_token']
                    node_name = 'Page- {0}'.format(page['name'])
            if page_access_token is None:
                log_message = '[{0}] Invalid publish page request'.format(get_current_formatted_datetime())
                helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.ERROR.value, log_message)
                return JsonResponse({
                    'status_code': 500,
                    'message': 'You don\'t have permission to publish post on this Page.'
                })

            access_token = page_access_token

        upload_api_service = settings.UPLOAD_API_SERVICE[upload_asset_detail.upload_to]

        # Type of media path 'local' or 'http'
        media_path_type = upload_api_service['media_path_type']
        # media_path_type = 'local'  # remove it later
        if media_path_type == 'local':
            media_file = upload_asset_detail.asset_detail.asset_source_unquoted_url
        else:
            media_file = os.path.join(
               download_directory_path(upload_asset_detail.asset_detail.id, True),
               upload_asset_detail.asset_detail.asset_source_decoded_url.split("/")[-1]
            )

            if not os.path.isfile(media_file):
                log_message = '[{0}] Downloading asset..'.format(get_current_formatted_datetime())
                helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.QUEUED.value, log_message)

                # Download media asset from provided asset url link
                media_file = helper.download_asset_to_upload(
                    upload_asset_detail.asset_detail.id,
                    upload_detail_id,
                    upload_asset_detail.asset_detail.asset_source_decoded_url
                )

        mime_type = helper.guess_mime_type(media_file)
        error = False
        if mime_type is not None:

            """
            Add fb account info in upload asset detail table
            """
            email_variable = None
            if 'email_variable' in upload_api_service:
                email_variable = upload_api_service['email_variable']

            social_identity = social_token.account.get_provider_account()
            if email_variable:
                extra_data = social_token.account.extra_data
                account_identifier = (email_variable in extra_data and str(extra_data[email_variable])) or str(extra_data['name'])
                social_identity = str(social_identity) + ' (' + account_identifier + '): ' + node_name

            upload_asset_detail = UploadAssetDetail.objects.get(id=upload_detail_id)
            upload_asset_detail.upload_to_account = social_identity
            log_message = '[{0}] Uploading your facebook post..'.format(get_current_formatted_datetime())
            upload_asset_detail.social_account_id = fb_account_id
            upload_asset_detail.upload_status = UploadAssetDetail.UploadStatus.UPLOADING.value
            upload_asset_detail.message = upload_asset_detail.message + log_message + "\n"
            upload_asset_detail.save()

            if mime_type == 'image':
                """
                A photo must be less than 10MB in size.
                """
                path = "{0}/photos".format(node_id)

                log_message = graph.put_photo(
                    image=open(media_file, 'rb'), album_path=path, caption=title, access_token=access_token
                )
            elif mime_type == 'video':
                log_message = put_video(media_file, node_id, title, access_token)
            else:
                error = True
                log_message = '[Error] [{0}] Unsupported Media file format: {1}'.format(
                    get_current_formatted_datetime(), mime_type
                )
        else:
            error = True
            log_message = '[Error] [{0}] Media file format is unknown.'.format(get_current_formatted_datetime())

        if error is True:
            helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.INVALID.value, log_message)
            return JsonResponse({
                'status_code': 0,
                'message': 'Attached media file format is unknown.'
            })
        else:
            if 'id' in log_message:
                message = '[{0}] Post has been uploaded successfully.'.format(get_current_formatted_datetime())
                status_code = 201
                response_message = 'Post has been uploaded successfully'
                helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.UPLOADED.value, message, log_message['id'])
                if media_path_type == 'http':
                    helper.silent_remove(media_file)
            else:
                status_code = 500
                response_message = 'Could not post to facebook. Please try again later.'
                message = '[Error] [{0}] {1}.'.format(get_current_formatted_datetime(), log_message)
                helper.update_asset_status(upload_detail_id, UploadAssetDetail.UploadStatus.ERROR.value, message)
                logger.error(message)
            return JsonResponse({
                'status_code': status_code,
                'message': response_message
            })
    except facebook.GraphAPIError as ex:
        exception = True
        error_message = ex
        upload_status = UploadAssetDetail.UploadStatus.ERROR.value
        response_message = 'Could not post to facebook. Please try again later.'
        status_code = 500
    except Exception as ex:
        exception = True
        error_message = ex
        upload_status = UploadAssetDetail.UploadStatus.INVALID.value
        response_message = 'Please contact your administrator for detail.'
        status_code = 0

    if exception is True:
        if upload_detail_id is not None:
            log_message = '[Severe] [{0}] {1}'.format(get_current_formatted_datetime(), error_message)
            helper.update_asset_status(upload_detail_id, upload_status, log_message)

        logger.exception(error_message)

        return JsonResponse({
            'status_code': status_code,
            'message': response_message
        })

def put_video(local_video_file, node_id, video_title, access_token):
    """ Non-Resumable Upload
    Post video on facebook using multipart/form-data

    Limitations: Non-Resumable upload supports video uploads that are up to 1GB and 20 minutes long.

    Supported Formats: The aspect ratio of the video must be between 9x16 and 16x9. We support the following formats
    for uploaded videos:
    3g2, 3gp, 3gpp, asf, avi, dat, divx, dv, f4v, flv, gif, m2ts, m4v, mkv, mod, mov, mp4, mpe, mpeg, mpeg4, mpg, mts,
    nsv, ogm, ogv, qt, tod, ts, vob, and wmv.

    :param string local_video_file: local path of video to be uploaded
    :param string|int node_id: it could be 'me'as string when posting on own timeline
                                or 'page-id' as integer value when posting on page
    :param string video_title: title for video
    :param string access_token: user-access-token for timeline or page-access-token for page
    :return: json response
    """

    video_file_name = local_video_file.split(os.sep)[-1]
    path = "{0}/videos".format(node_id)
    fb_url = "https://graph-video.facebook.com/{0}?access_token={1}".format(path, access_token)

    # multipart chunked uploads
    encoder = MultipartEncoder(
        fields={
            'title': video_title,
            'description': video_title,
            'source': (video_file_name, open(local_video_file, 'rb'))
        }
    )

    r = requests.post(fb_url, headers={'Content-Type': encoder.content_type}, data=encoder)

    return r.json()

'''
@login_required
def getty_login(request):

    try:
        # test_getty_redirect_url = 'https://api.gettyimages.com/SwaggerUI/implicit_grant.html'

        getty_redirect_url = reverse('getty_callback')
        getty_account = ClientAccounts.objects.get(client_name=ClientAccounts.Client.GETTY.value)
        getty_login_url = '{0}&client_id={1}&redirect_uri={2}'.format(
            settings.GETTY_LOGIN_URL,
            getty_account.client_id,
            getty_redirect_url
        )
        return redirect(getty_login_url)
    except Exception as ex:
        logger.exception(ex)
        messages.error(request, 'Getty Account is not configured yet! Please contact your admin.')

        return redirect('home')
    
@login_required
def getty_login_callback(request):
    """
    Callback url handler for Getty access token
    As Getty Server returns access token on this view in a query string but using '#' instead of '?'
    So this view is being captured by javascript in base.html file to get the `hash part of url` and
    send it to getty_login_token() view

    :param request:
    """

    return render(request, 'get_getty_access_token.html')

@login_required
def getty_login_token(request):
    """
    Internal callback handler for retrieving Getty login access token from query string

    :param request:
    :return:
    """

    access_token = request.GET.get('access_token')
    status = 0
    if access_token:

        try:
            access_token = urlparse.unquote(access_token)
            expires_seconds = int(request.GET.get('expires_in', '0').encode())
            expires_in = datetime.now() + timedelta(seconds=expires_seconds)
            token_type = request.GET.get('token_type', '')
            user_id = request.user.id

            # Fetch Getty user basic info
            getty_account = ClientAccounts.objects.get(client_name=ClientAccounts.Client.GETTY.value)
            headers = {
                'Authorization': 'Bearer ' + access_token,
                'Api-Key': getty_account.client_id,
                'Content-Type': 'application/json',
            }

            response = requests.get(settings.GETTY_USER_URL, headers=headers)

            if response.status_code == 200:
                extra_data = response.json()
                getty_user_id = extra_data.get('id')

                if getty_user_id:
                    try:
                        getty_token = GettyTokens.objects.get(uid=getty_user_id)

                        if request.user.id == getty_token.user.id:
                            getty_token.access_token = access_token
                            getty_token.token_type = token_type
                            getty_token.extra_data = extra_data
                            getty_token.expires_at = expires_in

                            getty_token.save()
                            status = 1
                            message = 'The Getty account has been connected.'
                        else:
                            message = 'The Getty account is already connected to a different user.'
                            status = 0
                    except GettyTokens.DoesNotExist:
                        # connect account
                        GettyTokens.objects.create(
                            user_id=user_id, access_token=access_token, token_type=token_type,
                            extra_data=extra_data, uid=getty_user_id, expires_at=expires_in
                        )
                        status = 1
                        message = 'The Getty account has been connected.'
                else:
                    message = "The Getty error while fetching user info."
            else:
                message = "The Getty error while fetching user info."
        except Exception as ex:
            logger.exception(ex)
            message = 'Internal server error while connecting to the Getty account.'
    else:
        message = 'Could not get access token of the Getty user.'
        logger.error(message)

    return JsonResponse({'message': message, 'status': status})

@login_required
@require_http_methods(["POST"])
def getty_account_disconnect(request):
    try:
        getty_token_id = request.POST.get('getty_token_id')
        user_id = request.POST.get('id')

        if getty_token_id and user_id:
            if request.user.id == int(user_id):
                GettyTokens.objects.get(id=int(getty_token_id)).delete()
                messages.info(request, 'The getty account has been disconnected.')
            else:
                messages.error(request, 'Invalid request.')
    except GettyTokens.DoesNotExist:
        messages.error(request, 'The getty account was not found.')
    except Exception:
        messages.error(request, 'Error while disconnecting the account. Please contact your admin.')

    return redirect('home')'''
