from allauth.socialaccount.models import SocialAccount
from smm_app import settings
from datetime import datetime
import os
from django.urls import reverse
from django.db import models
from allauth.account.adapter import DefaultAccountAdapter
from django.contrib.auth.models import User
from django.utils import timezone
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

class SocialAccountMembers(models.Model):
    social_account = models.ForeignKey(SocialAccount, on_delete=models.CASCADE, null=True)
    #getty_account = models.ForeignKey(GettyTokens, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    is_active = models.BooleanField(default=1)
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    updated_at = models.DateTimeField(null=True, blank=True, editable=False)
    user_username = models.TextField(null=True)

class AppAccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return False

class AppSocialAccountAdapter(DefaultSocialAccountAdapter):
    def get_connect_redirect_url(self, request, socialaccount):
        return reverse('home')



def download_directory_path(metadata_asset_id, for_upload_asset=False):
    """
    :param int metadata_asset_id: metadata-id or upload-asset-detail-id
    :param bool for_upload_asset:
    :return: (string) file-path
    """
    if for_upload_asset is False:
        directory_prefix = settings.METADATA_DIRECTORY_PREFIX
    else:
        directory_prefix = settings.ASSET_DIRECTORY_PREFIX

    # Path will be {settings.MEDIA_DOWNLOADS}/directory_prefix<metadata_asset_id> with 0755 permission
    asset_dir = os.path.join(settings.MEDIA_DOWNLOADS, directory_prefix + '{0}'.format(metadata_asset_id))
    if not os.path.isdir(asset_dir):
        try:
            os.makedirs(asset_dir, 0755)
        except OSError:
            pass

    return asset_dir

def get_current_formatted_datetime():
    return datetime.now().strftime(settings.DATETIME_DISPLAY_FORMAT)

