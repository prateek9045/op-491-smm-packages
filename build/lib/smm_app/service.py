# Client upload requests handler
import logging
import mimetypes
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.contrib import messages
import os
import time
from datetime import datetime
import socket

import requests
from allauth.socialaccount.models import SocialAccount
from django.db.models import Q

import settings

from models import SocialAccountMembers, download_directory_path


def get_current_formatted_datetime():
    return datetime.now().strftime(settings.DATETIME_DISPLAY_FORMAT)

log_messages = []
asset_status = []

def smm_request(social_platform, http_asset, user_id, asset_source_decoded_url):

    if social_platform != '':
        try:
            social_platform = str(social_platform).strip().lower()
            upload_api_service = settings.UPLOAD_API_SERVICE[social_platform]

            media_path_type = upload_api_service['media_path_type']
            if (media_path_type == 'local' and http_asset is True) \
                    or (media_path_type == 'http' and http_asset is False):
                print http_asset
                log_messages.append("Social Platform '{0}' is configured for accepting only '{1}'"
                            " paths".format(social_platform, media_path_type))
                #continue
                return [False, log_messages]

            if not is_media_type_allowed(
                    asset_source_decoded_url, upload_api_service
            ):
                #continue
                return [False, log_messages]

            service_provider = upload_api_service['provider']
            email_variable = None
            if 'email_variable' in upload_api_service:
                email_variable = upload_api_service['email_variable']
            # Get user's social accounts
            social_accounts = retrieve_social_member_accounts(user_id=user_id,
                                                              upload_api_service_provider=service_provider)

            """
            Special case for facebook
            """
            
            if social_platform == 'facebook':
                return [social_platform, log_messages]

            else:
                if social_accounts.count() == 0:
                    log_messages.append("User requested social platform '{0}' account is not connected "
                                        "with the server.".format(social_platform))

                else:
                    return [social_accounts, log_messages, 'social_accounts', email_variable]


        except KeyError:
            # Invalid social platform
            log_message = "[Error] [{0}] User requested social platform '{1}' could not be " \
                          "recognized by the server.".format(
                get_current_formatted_datetime(), social_platform
            )
            #update_asset_log(asset_detail_object, log_message, AssetLog=AssetLog)

            log_messages.append("User requested social platform '{0}' could not be recognized "
                         "by the server.".format(social_platform))

            return [False, log_message, 'update_asset_log']


def is_media_type_allowed(resource, upload_api_service):
    """
    Check if received asset is applicable for requested social platform

    :param string resource:
    :param dict upload_api_service:
    :return: bool
    """
    try:
        mime_type = guess_mime_type(resource)
        if 'allowed_media' not in upload_api_service or upload_api_service['allowed_media'] == mime_type:
            return True
    except Exception:
        pass

    log_messages.append('Invalid extension of asset: {0}.'.format(resource))

    return False


def guess_mime_type(media_file):
    """
    Guess mime type of a given file
    :param string media_file:
    :return:
    """
    mime_type = mimetypes.guess_type(media_file)[0]
    if mime_type is not None:
        return mime_type.split('/')[0]
    return None

# Retrieving social member accounts
def retrieve_social_member_accounts(user_id, upload_api_service_provider):
    try:
        social_member_accounts = SocialAccountMembers.objects.filter(user_id=user_id, is_active=1)
        social_account_ids = []
        for social_member_account in social_member_accounts:
            social_account_ids.append(social_member_account.social_account_id)

        social_accounts = SocialAccount.objects.filter(
            Q(Q(user_id=user_id) | Q(id__in=social_account_ids))
            & Q(provider=upload_api_service_provider)
        )
        return social_accounts
    except Exception as ex:
        log_messages.append(ex)

def update_asset_log(asset_detail, log_message, AssetLog):
    """
    Add/Update asset log message in database

    :param AssetDetail asset_detail:
    :param str log_message:
    :return: (object) AssetLog
    """

    try:
        asset_log = AssetLog.objects.get(asset_detail=asset_detail)
        asset_log.message = asset_log.message + log_message + "\n"
        asset_log.save()
    except AssetLog.DoesNotExist:
        asset_log = AssetLog.objects.create(asset_detail=asset_detail, message=log_message)

    return asset_log


def download_asset_to_upload(asset_detail_id, upload_asset_detail_id, asset_source_decoded_url):
    """
    Download asset first in order to upload it to another server( - mainly social media platform)

    :param int asset_detail_id:
    :param int upload_asset_detail_id:
    :param string asset_source_decoded_url:
    :return: downloaded-file-path
    """
    asset_file_name = asset_source_decoded_url.split("/")[-1]

    download_to = os.path.join(
       download_directory_path(asset_detail_id, True),
       asset_file_name
    )
    print download_to
    is_downloaded = False

    for i in range(settings.DOWNLOAD_MAX_RETRIES):
        try:
            response = requests.get(asset_source_decoded_url, stream=True, timeout=10)
            # Handle bad http errors
            response.raise_for_status()
            with open(download_to, 'wb') as f:
                for chunk in response.iter_content(chunk_size=1024):
                    # dl += len(chunk)
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)

                log_message = '[{0}] Asset downloaded successfully.'.format(get_current_formatted_datetime())
                #update_upload_log(upload_asset_detail_id, log_message)
                print 'success'
                log_messages.append(log_message)
                is_downloaded = True
            # Break max-retry loop if downloaded successfully
            break
        except requests.exceptions.ConnectionError:
            print requests.exceptions.ConnectionError
            # Raised when http connection failed
            # Log message in database if http connection failed
            log_message = "[Error] [{0}] Http Connection failed while downloading asset. " \
                          "Retrying in {1} seconds".format(
                                get_current_formatted_datetime(), settings.DOWNLOAD_NEXT_RETRY_IN
                            )
            #update_upload_log(upload_asset_detail_id, log_message)
            log_messages.append(log_message)

        except socket.timeout:
            # Raised when connection lost while streaming content from http url
            # Log message in database
            log_message = "[Error] [{0}] Http Connection socket timeout. " \
                          "Retrying after {1} seconds".format(
                                get_current_formatted_datetime(), settings.DOWNLOAD_NEXT_RETRY_IN
                            )
            #update_upload_log(upload_asset_detail_id, log_message)
            log_messages.append(log_message)

        time.sleep(settings.DOWNLOAD_NEXT_RETRY_IN)
    else:
        # Connection error, max-retry exceeded
        log_message = "[Severe] [{0}] Download failed! Max retry reached!".format(
            get_current_formatted_datetime()
        )
        #update_asset_status(upload_asset_detail_id, UploadAssetDetail.UploadStatus.ERROR.value, log_message)\
        asset_status.append('error')

    if is_downloaded is True:
        return download_to
    else:
        return None

# Restricting superuser to login on front-end panel.
def restrict_superuser_to_login(request):
    superusers_usernames = User.objects.filter(is_superuser=True).values_list('username')
    for superusers_username in superusers_usernames:
        if str(superusers_username[0]) == str(request.user.username):
            logout(request)
            messages.error(request, "You don't have access to this panel.")
            return redirect('login')

# Exclude linked users
def exclude_added_users(excluded_current_user_and_admin, linked_user_list):
    excluded_users = []
    try:
        if len(linked_user_list)>0:
            for user in excluded_current_user_and_admin:
                excluded_users.append(user)
            for x in linked_user_list:
                for user in excluded_users:
                    if not x.user:
                        user_email = str(x.user_username)
                    else:
                        user_email = str(x.user.username)
                    if str(user) == user_email:
                        excluded_users.remove(user)
        else:
            excluded_users.append(excluded_current_user_and_admin)
            return excluded_users[0]
        return excluded_users
    except Exception as ex:
        #logger.error(ex)
        print ex